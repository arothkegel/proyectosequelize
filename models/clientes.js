'use strict';
module.exports = (sequelize, DataTypes) => {
  const clientes = sequelize.define('clientes', {
    nombre: DataTypes.STRING,
    apelido: DataTypes.STRING,
    direccion: DataTypes.STRING,
    direccion2: DataTypes.STRING,
    empresa: DataTypes.STRING,
    codigoPostal: DataTypes.INTEGER,
    ciudad: DataTypes.STRING,
    comuna: DataTypes.STRING
  }, {});
  clientes.associate = function(models) {
    // associations can be defined here
  };
  return clientes;
};