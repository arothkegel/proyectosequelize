var express = require('express');
var router = express.Router();

const Cliente = require('./../models').clientes;

/* GET users listing. */
router.get('/', function (req, res, next) {
  Cliente.findAll({}).then(function (clientes) {
    console.log('clientes', clientes);
    res.send(clientes);
  });

});


router.post('/guardarCliente', function (req, res, next) {
  console.log('datos del post', req.body);


  Cliente.create({
    nombre: req.body.nombre,
    apelido: req.body.apellido,
    direccion: req.body.direccion,
    direccion2: req.body.direccion2,
    empresa: req.body.empresa,
    codigoPostal: req.body.codigoPostal,
    ciudad: req.body.ciudad,
    comuna: req.body.comuna,
  }).then(function (cliente) {
    console.log(cliente);
    res.send(cliente);
  });


});


router.put('/modificaCliente/:id', function (req, res, next) {
  // let id = req.params.id;
  Cliente.find({
    where: {
      id: req.params.id
    }

  }).then(function (clientes) {

    clientes.updateAttributes({
        nombre: req.body.nombre,
        apelido: req.body.apellido,
        direccion: req.body.direccion,
        direccion2: req.body.direccion2,
        empresa: req.body.empresa,
        codigoPostal: req.body.codigoPostal,
        ciudad: req.body.ciudad,
        comuna: req.body.comuna,
      })
      .then(function (clienteActualizado) {
        res.send(clienteActualizado);

      });
  });

});

router.delete('/eliminaCliente/:id', function (req, res, next) {

    Cliente.destroy({
      where:{
        id: req.params.id
      }
    }).then(function(objetoEliminado){
      res.send(200);
    });

});

module.exports = router;